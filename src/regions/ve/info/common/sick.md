## 🤒 ¿Qué pasa si estoy enfermo?

### ¿Cuáles son los síntomas de COVID-19?
Por lo general, los coronavirus humanos causan enfermedades respiratorias de leves a moderadas. Los síntomas son muy similares a la gripe, que incluyen:

- Fiebre
- Tos
- Falta de aire o dificultad para respirar

El COVID-19 puede causar una enfermedad respiratoria más severa.

### ¿Qué pasa si tengo síntomas?

Si una persona desarrolla síntomas de COVID-19, como fiebre, tos o falta de aire, y tiene razones para creer que puede haber estado expuesta, debes llamar a tu médico antes de buscar atención médica para que se puedan tomar las precauciones adecuadas. Contactarlos con anticipación asegurará que las personas puedan obtener la atención que necesitan sin poner en riesgo a otros. También puede tomar las siguientes medidas de precaución: evite el contacto con personas enfermas, lávese las manos frecuentemente con agua tibia y jabón durante al menos 20 segundos.
