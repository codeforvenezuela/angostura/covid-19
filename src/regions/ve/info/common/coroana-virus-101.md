## 🦠 ¿Qué es el COVID-19?

La enfermedad del coronavirus 2019 (COVID-19) es una afección respiratoria que se puede propagar de persona a persona. El virus que causa el COVID-19 es un nuevo coronavirus que se identificó por primera vez durante la investigación de un brote en Wuhan, China.


