## 👨‍👩‍👧 Protégete

### ¿Cómo pueden protegerse las personas?

Actualmente no existe una vacuna para prevenir COVID-19. La mejor manera de prevenir enfermedades es evitar la exposición a este virus. Se cree que el virus se transmite principalmente de persona a persona entre gente que están en contacto cercano entre sí (dentro de unos 6 pies). Esto ocurre a través de gotas respiratorias producidas cuando una persona infectada tose o estornuda. Estas gotitas pueden caer en la boca o la nariz de las personas cercanas o posiblemente ser inhaladas a los pulmones. Los adultos mayores y las personas que tienen afecciones médicas crónicas subyacentes graves, como enfermedad cardíaca o pulmonar o diabetes, parecen tener un mayor riesgo de desarrollar complicaciones más graves por la enfermedad COVID-19. Cada persona tiene un papel que desempeñar. Gran parte de protegerse a sí mismo y a su familia se reduce al sentido común:

- Lavarse las manos con agua y jabón.
- Limpiar y desinfectar diariamente superficies que toca frecuentemente.
- Evitar tocarse los ojos, la nariz o la boca sin lavarse las manos.
- Cubrirse al toser o estornudar con un pañuelo desechable o con su codo.
- Evitar el contacto cercano con personas enfermas.
- Mantenerse alejado del trabajo, la escuela u otras personas si se enferma y presenta síntomas respiratorios como fiebre y tos.

Consulte con tu médico sobre los pasos adicionales que puedes tomar para protegerte

### ¿Quién está en mayor riesgo de enfermedad grave por causa de COVID-19?

La información preliminar sobre el COVID-19 muestra que algunas personas tienen un mayor riesgo de enfermarse gravemente por esta enfermedad. Esto incluye:

- Adultos mayores
- Individuos con sistemas inmunes comprometidos
- Las personas que tienen afecciones médicas crónicas graves, como:
- Enfermedades del corazón
- Diabetes
- Enfermedades pulmonar

Si tiene un mayor riesgo de de contraer una enfermedad grave debido a COVID-19 por su edad o estado de salud, es importante que tome medidas para reducir su riesgo de enfermarse con la enfermedad. Las acciones que puede tomar para reducir su riesgo incluyen:

- Lávese las manos con frecuencia con agua y jabón por al menos 20 segundos, especialmente después de sonarse la nariz, toser o estornudar, o haber estado en un lugar público.
- Evitar tocarse los ojos, la nariz o la boca sin lavarse las manos.
- Evite el contacto cercano con personas enfermas y manténgase alejado de grandes reuniones y multitudes.
- Quédate en casa tanto como sea posible.
