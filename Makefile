.PHONY: build
build:
	export REACT_APP_COMMIT_SHA1=`git rev-parse HEAD`; \
	export REACT_APP_SHARE_PUBLIC_URL="https://covidvzlabot.codeforvenezuela.org"; \
	npm run build
